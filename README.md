# Source & Binary

Get the source with git with:
```
git clone https://git.savannah.nongnu.org/git/blu-save.git
```
or download the tarball from https://download.savannah.nongnu.org/releases/blu-save/

# Building

Make sure you have `libbluray>=1.2.0`, after that you can build blu-save with just
```
$ make
```

# License

```
blu-save helps you to backup your Blu-ray discs
Copyright 2019 Yunxiang Li

This file is part of blu-save.

blu-save is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

blu-save is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with blu-save.  If not, see <https://www.gnu.org/licenses/>.
```
