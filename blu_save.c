/*
 * blu-save helps you to backup your Blu-ray discs
 * Copyright 2019 Yunxiang Li
 *
 * This file is part of blu-save.
 *
 * blu-save is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * blu-save is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <pthread.h>
#include <stdbool.h>
#include <libbluray/bluray.h>
#include <libbluray/filesystem.h>

#include "blu_save.h"

typedef struct
{
  uint8_t *full_buf;
  int64_t len;
  uint8_t *empty_buf;
  FILE *fp_out;
  int write_error;

  pthread_mutex_t *mutex;
  pthread_cond_t *can_read;
  pthread_cond_t *can_write;
} queue_t;

void
file_close (BD_FILE_H *file)
{
  file->close (file);
}

int64_t
file_read (BD_FILE_H *file, uint8_t *buf, int64_t size)
{
  return file->read (file, buf, size);
}

void
dir_close (BD_DIR_H *dir)
{
  dir->close (dir);
}

int
dir_read (BD_DIR_H *dir, BD_DIRENT *entry)
{
  return dir->read (dir, entry);
}

void *
writer (void *arg)
{
  queue_t *q = arg;
  FILE *fp_out = q->fp_out;
  bool end = false;
  while (!end && !(q->write_error))
    {
      pthread_mutex_lock (q->mutex);
      if (q->full_buf == NULL)
        {
          pthread_cond_wait (q->can_write, q->mutex);
        }
      uint8_t *buf = q->full_buf;
      int64_t len = q->len;
      q->full_buf = NULL;
      pthread_mutex_unlock (q->mutex);

      int error = fwrite (buf, 1, len, fp_out) != len;
      // we'll call fwrite after EOF. It is used to check for errors.
      if (len < BUF_SIZE)
        {
          error += fwrite (buf, 1, 0, fp_out) != 0;
          end = true;
        }

      if (error)
        {
          pthread_mutex_lock (q->mutex);
          q->write_error = error;
          pthread_mutex_unlock (q->mutex);
        }
      else if (!end)
        {
          pthread_mutex_lock (q->mutex);
          if (q->empty_buf != NULL)
            {
              pthread_cond_wait (q->can_write, q->mutex);
            }
          q->empty_buf = buf;
          pthread_cond_signal (q->can_read);
          pthread_mutex_unlock (q->mutex);
        }
    }
  return NULL;
}

void
dump_file (BLURAY *bd, const char *rel_path, const char *cache_path,
           void *error_sink, volatile const bool *stop, queue_t *q)
{
  BD_FILE_H *fp_in;
  FILE *fp_out;

  // open input file
  if (!(fp_in = bd_open_file_dec (bd, rel_path)))
    {
      show_error (error_sink, "failed to open %s\n", rel_path);
      return;
    }

  // open output file
  if (!(fp_out = fopen (cache_path, "wb")))
    {
      file_close (fp_in);
      show_error (error_sink, "failed to create %s\n", cache_path);
      return;
    }

  uint8_t *buf_a = malloc (BUF_SIZE);
  uint8_t *buf_b = malloc (BUF_SIZE);
  q->full_buf = buf_a;
  q->empty_buf = buf_b;
  q->fp_out = fp_out;

  // write to output file
  pthread_t writer_id;
  int64_t read, got = 0;
  for (uint8_t *ptr = q->full_buf; ptr < q->full_buf + BUF_SIZE;
       ptr += BLOCK_SIZE)
    {
      read = file_read (fp_in, ptr, BLOCK_SIZE);
      if (read > 0)
        got += read;
      else
        {
          if (read < 0)
            {
              remove (cache_path);
              show_error (error_sink, "failed to cache %s\n", rel_path);
            }
          break;
        }
    }
  if (got > 0)
    {
      q->len = got;
      pthread_create (&writer_id, NULL, writer, (void *) q);
    }
  while (got > 0 && read > 0)
    {
      got = 0;
      pthread_mutex_lock (q->mutex);
      if (q->empty_buf == NULL)
        {
          pthread_cond_wait (q->can_read, q->mutex);
        }
      uint8_t *buf = q->empty_buf;
      q->empty_buf = NULL;
      pthread_mutex_unlock (q->mutex);
      for (uint8_t *ptr = buf; ptr < buf + BUF_SIZE; ptr += BLOCK_SIZE)
        {
          read = file_read (fp_in, ptr, BLOCK_SIZE);
          if (read > 0)
            got += read;
          else
            break;
        }
      pthread_mutex_lock (q->mutex);
      if (*stop || read < 0 || q->write_error)
        {
          remove (cache_path);
          show_error (error_sink, "failed to cache %s\n", rel_path);
          pthread_mutex_unlock (q->mutex);
          break;
        }
      if (q->full_buf != NULL)
        {
          pthread_cond_wait (q->can_read, q->mutex);
        }
      q->full_buf = buf;
      q->len = got;
      pthread_cond_signal (q->can_write);
      pthread_mutex_unlock (q->mutex);
    }

  pthread_join (writer_id, NULL);
  free (buf_b);
  free (buf_a);
  fclose (fp_out);
  file_close (fp_in);
}

static size_t
new_path_size (const char *path)
{
  return strlen (path) + strlen (DIR_SEP) + BD_NAME_MAX + 1;
}

void
dump_dir (BLURAY *bd, const char *base_dir, const char *out_dir,
          void *error_sink, volatile const bool *stop, queue_t *q)
{
  BD_DIRENT ep;
  BD_DIR_H *dp;

  if (!(dp = (BD_DIR_H *) bd_open_dir (bd, base_dir)))
    {
      show_error (error_sink, "failed to open %s\n", base_dir);
      return;
    }

  if (dir_read (dp, &ep) == 0)
    { // non-empty dir
      char *base = malloc (new_path_size (base_dir));
      char *out = malloc (new_path_size (out_dir));

      mkdir (out_dir, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH); // drwxrwxr-x
      do
        {
          char *name = ep.d_name;
          if (*base_dir)
            strcat (strcat (strcpy (base, base_dir), DIR_SEP), name);
          else
            strcpy (base, name);

          if (*out_dir)
            strcat (strcat (strcpy (out, out_dir), DIR_SEP), name);
          else
            strcpy (out, name);

          if (strchr (name, '.'))
            {
              if (strlen (name) > 2)
                dump_file (bd, base, out, error_sink, stop, q);
            }
          else
            {
              dump_dir (bd, base, out, error_sink, stop, q);
            }
        }
      while (!(*stop) && dir_read (dp, &ep) == 0);
      free (out);
      free (base);
    }
  dir_close (dp);
}

void
blu_save_dump_bd (const char *disc_path, const char *out_path,
                  const char *key_path, void *error_sink,
                  volatile const bool *stop)
{
  BLURAY *bd;
  if (!(bd = bd_open (disc_path, key_path)))
    {
      show_error (error_sink, "failed to open %s\n", disc_path);
      return;
    }
  if (!(out_path && *out_path))
    out_path = DEFAULT_OUTDIR;

  queue_t q;
  q.len = 0;
  q.write_error = 0;
  q.mutex = malloc (sizeof (pthread_mutex_t));
  q.can_read = malloc (sizeof (pthread_cond_t));
  q.can_write = malloc (sizeof (pthread_cond_t));
  pthread_mutex_init (q.mutex, NULL);
  pthread_cond_init (q.can_read, NULL);
  pthread_cond_init (q.can_write, NULL);

  dump_dir (bd, "", out_path, error_sink, stop, &q);

  pthread_cond_destroy (q.can_write);
  pthread_cond_destroy (q.can_read);
  pthread_mutex_destroy (q.mutex);
  free (q.can_write);
  free (q.can_read);
  free (q.mutex);
  bd_close (bd);
}
