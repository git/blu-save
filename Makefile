# blu-save helps you to backup your Blu-ray discs
# Copyright 2019 Yunxiang Li
#
# This file is part of blu-save.
#
# blu-save is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# blu-save is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with blu-save.  If not, see <https://www.gnu.org/licenses/>.

CC = cc
CFLAGS = -g -Os -pedantic -Wall
LIBBR_H = `pkg-config --cflags libbluray`
LIBBR_L = `pkg-config --libs libbluray`
GTKLIB_H = `pkg-config --cflags gtk+-3.0`
GTKLIB_L = `pkg-config --libs gtk+-3.0`
VERSION = `git tag | tail -n1`

blu-save : cli.c blu-save.o
	${CC} ${CFLAGS} ${LIBBR_H} ${LIBBR_L} -lpthread $^ -o $@

blu-save-gtk : gtk.c blu-save.o
	${CC} ${CFLAGS} -rdynamic ${LIBBR_H} ${GTKLIB_H} ${LIBBR_L} ${GTKLIB_L} $^ -o $@

blu-save.o : blu_save.c blu_save.h
	${CC} -c -o $@ $< ${CFLAGS}

.PHONY : clean cli gtk all

clean :
	rm blu-save*

cli : blu-save
gtk : blu-save-gtk
all : cli gtk

release : all
	strip blu-save blu-save-gtk
	tar -czvf blu-save-${VERSION}.tar.gz ../blu-save/{blu-save{,-gtk},COPYING,README.md}
	tar -czvf blu-save-${VERSION}-src.tar.gz --exclude='../blu-save/blu-save*' ../blu-save/*
